/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2018 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "main.h"
#include "stm32f3xx_hal.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"
#include "ringbuffer_dma.h"
#include "can_open.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId readCANHandle;
osThreadId sendCANHandle;
osThreadId readUARTHandle;
osThreadId sendUARTHandle;

/* USER CODE BEGIN Variables */
osMailQId mCANRxFramesHandle;
osMailQId mCANTxFramesHandle;
volatile bool canopnd = 0;

#define RX_BUFFER_SIZE 1024
RingBufferDMA rx_uart_buffer;
uint8_t uart_buffer[RX_BUFFER_SIZE];
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartReadingCAN(void const * argument);
void StartSendingCAN(void const * argument);
void StartReadingUART(void const * argument);
void StartSendingUART(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void CAN_Config(void);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName) {
	/* Run time stack overflow checking is performed if
	 configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
	 called if a stack overflow is detected. */
	Error_Handler();
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void) {
	/* vApplicationMallocFailedHook() will only be called if
	 configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
	 function that will get called if a call to pvPortMalloc() fails.
	 pvPortMalloc() is called internally by the kernel whenever a task, queue,
	 timer or semaphore is created. It is also called by various parts of the
	 demo application. If heap_1.c or heap_2.c are used, then the size of the
	 heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	 FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	 to query the size of free heap space that remains (although it does not
	 provide information on how the remaining heap might be fragmented). */
	Error_Handler();
}
/* USER CODE END 5 */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */
	RingBufferDMA_Init(&rx_uart_buffer, huart2.hdmarx, uart_buffer, RX_BUFFER_SIZE);
	HAL_UART_Receive_DMA(&huart2, uart_buffer, RX_BUFFER_SIZE); // Start UART DMA receive
	CAN_Config();
	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the thread(s) */
	/* definition and creation of readCAN */
	osThreadDef(readCAN, StartReadingCAN, osPriorityNormal, 0, 256);
	readCANHandle = osThreadCreate(osThread(readCAN), NULL);

	/* definition and creation of sendCAN */
	osThreadDef(sendCAN, StartSendingCAN, osPriorityHigh, 0, 256);
	sendCANHandle = osThreadCreate(osThread(sendCAN), NULL);

	/* definition and creation of readUART */
	osThreadDef(readUART, StartReadingUART, osPriorityNormal, 0, 256);
	readUARTHandle = osThreadCreate(osThread(readUART), NULL);

	/* definition and creation of sendUART */
	osThreadDef(sendUART, StartSendingUART, osPriorityAboveNormal, 0, 256);
	sendUARTHandle = osThreadCreate(osThread(sendUART), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* Create the queue(s) */
	/* definition and creation of mCANRxFrames */
	osMailQDef(mCANRxFrames, 32, CanRxMsgTypeDef);
	mCANRxFramesHandle = osMailCreate(osMailQ(mCANRxFrames), NULL);
	if (mCANRxFramesHandle == NULL) {
		Error_Handler(); // memory pool NOT created
	}

	/* definition and creation of mCANTxFrames */
	osMailQDef(mCANTxFrames, 32, CanTxMsgTypeDef);
	mCANTxFramesHandle = osMailCreate(osMailQ(mCANTxFrames), NULL);
	if (mCANTxFramesHandle == NULL) {
		Error_Handler(); // memory pool NOT created
	}
	/* USER CODE END RTOS_QUEUES */
}

/* StartReadingCAN function */
void StartReadingCAN(void const * argument) {

	/* USER CODE BEGIN StartReadingCAN */
	/* Infinite loop */
	for (;;) {
		HAL_StatusTypeDef status = HAL_ERROR;
		if (!canopnd) {
			osDelay(100);
			continue;
		}
		if (__HAL_CAN_MSG_PENDING(&hcan, CAN_FIFO0) > 0) {
			status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 1);
		} else if (__HAL_CAN_MSG_PENDING(&hcan, CAN_FIFO1) > 0) {
			status = HAL_CAN_Receive(&hcan, CAN_FIFO1, 1);
		}
		if (status == HAL_OK) {
			LED_ON();
			CanRxMsgTypeDef *frame = osMailAlloc(mCANRxFramesHandle, 0);
			if (frame != NULL) { // memory block was allocated
				memcpy(frame, hcan.pRxMsg, sizeof(CanRxMsgTypeDef));
				osMailPut(mCANRxFramesHandle, frame);
			}
			LED_OFF();
		} else {
			osThreadYield();
		}
	}
	/* USER CODE END StartReadingCAN */
}

/* StartSendingCAN function */
void StartSendingCAN(void const * argument) {
	/* USER CODE BEGIN StartSendingCAN */
	osEvent evt;
	/* Infinite loop */
	for (;;) {
		if (!canopnd) {
			osDelay(100);
			continue;
		}
		evt = osMailGet(mCANTxFramesHandle, osWaitForever); // wait for message
		if (evt.status == osEventMail) {
			LED_ON();
			CanTxMsgTypeDef *frame = evt.value.p;
			memcpy(hcan.pTxMsg, frame, sizeof(CanTxMsgTypeDef));
			HAL_CAN_Transmit(&hcan, 1);
			osMailFree(mCANTxFramesHandle, frame);
			LED_OFF();
		}
	}
	/* USER CODE END StartSendingCAN */
}

/* StartReadingUART function */
void StartReadingUART(void const * argument) {
	/* USER CODE BEGIN StartReadingUART */
	/* Infinite loop */
	for (;;) {
		if (RingBufferDMA_Count(&rx_uart_buffer) > 0) {
			CANOpen_UART_Parse(&rx_uart_buffer);
		} else {
			osThreadYield();
		}
	}
	/* USER CODE END StartReadingUART */
}

/* StartSendingUART function */
void StartSendingUART(void const * argument) {
	/* USER CODE BEGIN StartSendingUART */
	osEvent evt;
	/* Infinite loop */
	for (;;) {
		evt = osMailGet(mCANRxFramesHandle, osWaitForever); // wait for message
		if (evt.status == osEventMail) {
			CanRxMsgTypeDef * frame = evt.value.p;
			CANOpen_UART_Transmit(frame);
			osMailFree(mCANRxFramesHandle, frame);
		}
	}
	/* USER CODE END StartSendingUART */
}

/* USER CODE BEGIN Application */
void CAN_Config(void) {

	/* Allocate memory for incoming and outgoing messages */
	static CanTxMsgTypeDef TxMessage;
	static CanRxMsgTypeDef RxMessage;
	/* Set message holders */
	hcan.pTxMsg = &TxMessage;
	hcan.pRxMsg = &RxMessage;

	hcan.pTxMsg->ExtId = 0x0000;
	hcan.pTxMsg->StdId = 0x0000;
	hcan.pTxMsg->RTR = CAN_RTR_DATA; // data or remote request
	hcan.pTxMsg->IDE = CAN_ID_EXT; // 11 or 29 bits
	hcan.pTxMsg->DLC = 8; // payload in bytes [0..8]

	CAN_FilterConfTypeDef sFilterConfig;
	sFilterConfig.FilterNumber = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = 0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 14;

	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*
 * can_open.c
 *
 */

#include "can_open.h"

static uint8_t txCmd[100];
static uint8_t rxCmd[100];
static uint32_t i_rxCmd = 0;

volatile uint8_t cants = 0;
extern volatile bool canopnd;

extern osMailQId mCANTxFramesHandle;

static unsigned int hctoi(char c);

void CANOpen_UART_Transmit(CanRxMsgTypeDef *frame) {
	if (frame->IDE == CAN_ID_STD) {
		if (frame->RTR == CAN_RTR_DATA) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "t", 1, 1);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "r", 1, 1);
		}
		sprintf((char *) txCmd, "%03lx%lu", frame->StdId, frame->DLC);
	} else {
		if (frame->RTR == CAN_RTR_DATA) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "T", 1, 1);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "R", 1, 1);
		}
		sprintf((char *) txCmd, "%08lx%lu", frame->ExtId, frame->DLC);
	}
	HAL_UART_Transmit(&huart2, txCmd, strlen((char *) txCmd), 5);
	uint8_t i;
	for (i = 0; i < frame->DLC; ++i) {
		sprintf((char *) txCmd, "%02x", frame->Data[i]);
		HAL_UART_Transmit(&huart2, txCmd, 2, 5);
	}
	if (cants) {
		/* time stamp starts at 0x0000 and overflows at 0xEA5F which is equal to 59999ms */
		sprintf((char *) txCmd, "%04lx", HAL_GetTick() % 0xEA60);
		HAL_UART_Transmit(&huart2, txCmd, 2, 5);
	}
	HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
}

void CANOpen_UART_Parse(RingBufferDMA * rx_uart_buffer) {
	uint8_t rxByte = RingBufferDMA_GetByte(rx_uart_buffer);
	if (rxByte == '\r') {
		rxCmd[i_rxCmd] = 0;
		i_rxCmd = 0;
		CANOpen_Process_Command(rxCmd);
	} else if (rxByte == '\n') {
		return;
	} else {
		rxCmd[i_rxCmd++ % 100] = rxByte;
	}
}

void CANOpen_Process_Command(uint8_t * command) {
	if (command[0] == 'v') {	// sw ver
		HAL_UART_Transmit(&huart2, (uint8_t *) "v0100\r", 6, 10);
	} else if (command[0] == 'V') {	// hw ver
		HAL_UART_Transmit(&huart2, (uint8_t *) "V0100\r", 6, 10);
	} else if (command[0] == 'N') {	// Serial Num
		HAL_UART_Transmit(&huart2, (uint8_t *) "TM001\r", 6, 10);
	} else if (command[0] == 'C') {	// close
		canopnd = false;
		if (HAL_CAN_DeInit(&hcan) == HAL_OK) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	} else if (command[0] == 'W') {	// overflow flags
		// not implemented
		HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
	} else if (command[0] == 'S') {	// baudrate
		uint8_t speed = command[1] - '0';
		if (speed == 8)
			hcan.Init.Prescaler = 2; // 1Mbit
		if (speed == 6)
			hcan.Init.Prescaler = 4; // 500kb
		if (speed == 5)
			hcan.Init.Prescaler = 8; // 250kb
		if (speed == 4)
			hcan.Init.Prescaler = 16; // 125kb
		HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
	} else if (command[0] == 'Z') {	// timestamping on/off
		cants = !cants;
		HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
	} else if (command[0] == 'L') {	// open ro (listen)
		hcan.Init.Mode = CAN_MODE_SILENT;
		canopnd = true;
		if (HAL_CAN_Init(&hcan) == HAL_OK) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	} else if (command[0] == 'O') {	// open rw
		hcan.Init.Mode = CAN_MODE_NORMAL;
		canopnd = true;
		if (HAL_CAN_Init(&hcan) == HAL_OK) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	} else if (command[0] == 'I' || command[0] == 'l') {	// open loopback
		hcan.Init.Mode = CAN_MODE_LOOPBACK;
		canopnd = true;
		if (HAL_CAN_Init(&hcan) == HAL_OK) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	} else if (command[0] == 'D') {	// open loopback
		hcan.Init.Mode = CAN_MODE_SILENT_LOOPBACK;
		canopnd = true;
		if (HAL_CAN_Init(&hcan) == HAL_OK) {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	} else if ((command[0] == 't') || (command[0] == 'T') || (command[0] == 'r') || (command[0] == 'R')) { // tx
		uint8_t i;
		CanTxMsgTypeDef *frame = osMailAlloc(mCANTxFramesHandle, 0);
		if (frame != NULL) { // memory block was allocated
			frame->IDE = CAN_ID_STD; // default = std frame
			uint8_t idl = 3; // std id length = 3 nibbles (11 bits)
			if (command[0] < 'a') {	// if uppercase
				frame->IDE = CAN_ID_EXT;	// change to ext frame
				idl = 8;	// ext id length = 8 nibbles (29 bits)
			}
			frame->DLC = hctoi(command[1 + idl]);
			if (frame->DLC > 8) return;
			if (strlen((char *) command) != 1 + idl + 1 + 2 * frame->DLC) return;
			frame->ExtId = 0;
			for (i = 0; i < idl; ++i) {
				frame->ExtId = frame->ExtId * 16 + hctoi(command[1 + i]);
			}
			frame->StdId = frame->ExtId;	// dunno why there are separate fields for std and ext ID
			for (i = 0; i < frame->DLC; ++i) {
				frame->Data[i] = hctoi(command[1 + idl + 1 + 2 * i]) * 16 + hctoi(command[1 + idl + 1 + 2 * i + 1]);
			}
			frame->RTR = CAN_RTR_DATA;
			if ((command[0] == 'r') || (command[0] == 'R')) {
				frame->RTR = CAN_RTR_REMOTE;
			}
			osMailPut(mCANTxFramesHandle, frame);
			HAL_UART_Transmit(&huart2, (uint8_t *) "\r", 1, 10);
		} else {
			HAL_UART_Transmit(&huart2, (uint8_t *) "\a", 1, 10);
		}
	}
}

static unsigned int hctoi(char c) {
	if (c == '0')
		return (0);
	else if (c == '1')
		return (1);
	else if (c == '2')
		return (2);
	else if (c == '3')
		return (3);
	else if (c == '4')
		return (4);
	else if (c == '5')
		return (5);
	else if (c == '6')
		return (6);
	else if (c == '7')
		return (7);
	else if (c == '8')
		return (8);
	else if (c == '9')
		return (9);
	else if (c == 'a')
		return (10);
	else if (c == 'A')
		return (10);
	else if (c == 'b')
		return (11);
	else if (c == 'B')
		return (11);
	else if (c == 'c')
		return (12);
	else if (c == 'C')
		return (12);
	else if (c == 'd')
		return (13);
	else if (c == 'D')
		return (13);
	else if (c == 'e')
		return (14);
	else if (c == 'E')
		return (14);
	else if (c == 'f')
		return (15);
	else
		return (15);
}

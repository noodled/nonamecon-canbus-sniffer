/*
 * can_open.h
 *
 */

#ifndef CAN_OPEN_H_
#define CAN_OPEN_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "main.h"
#include "stm32f3xx_hal.h"
#include "can.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"
#include "ringbuffer_dma.h"

#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

void CANOpen_UART_Transmit(CanRxMsgTypeDef *frame);
void CANOpen_UART_Parse(RingBufferDMA * rx_uart_buffer);
void CANOpen_Process_Command(uint8_t * command);

#endif /* CAN_OPEN_H_ */

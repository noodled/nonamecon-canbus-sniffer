#ifndef RINGBUFFERDMA_H_INCLUDED
#define RINGBUFFERDMA_H_INCLUDED

#include <stdint.h>
#include "stm32f3xx_hal.h"

typedef struct
{
    uint8_t * data;
    uint32_t size;
    DMA_HandleTypeDef * hdma;
    uint8_t const * tail_ptr;
} RingBufferDMA;

void RingBufferDMA_Init(RingBufferDMA * buffer, DMA_HandleTypeDef * hdma, uint8_t * data, uint32_t size);
uint8_t RingBufferDMA_GetByte(RingBufferDMA * buffer);
uint32_t RingBufferDMA_Count(RingBufferDMA * buffer);

#endif /* RINGBUFFER_H_INCLUDED */
